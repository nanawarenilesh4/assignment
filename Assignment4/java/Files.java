import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Files {
	FileInputStream fi;
	FileOutputStream fo;
	XSSFWorkbook workbook;
	XSSFSheet sheet;
	XSSFRow row;
	XSSFCell cell;
	String path = null;

	Files(String path) {
		this.path = path;
	}

	public String getcellData(String sheetname, int rownum, int colnum) throws IOException {
		fi = new FileInputStream(path);
		workbook = new XSSFWorkbook(fi);
		sheet = workbook.getSheet(sheetname);
		row = sheet.getRow(rownum);
		cell = row.getCell(colnum);

//		/*
//		 * DataFormatter formatter = new DataFormatter(); String data; try { data =
//		 * formatter.formatCellValue(cell); // consider everything as string } catch
//		 * (Exception e) // if data is not available in the cell { data = ""; }
//		 */
		workbook.close();
		fi.close();
		/* return data; */
		return sheetname;
	}
	
	public void setcellData(String sheetname, int rownum, int colnum, String data) throws IOException {	
		File xf=new File(path);
		if(!xf.exists())    // if not exists create new
		{
		workbook =new XSSFWorkbook();
		fo=  new FileOutputStream(path);
		workbook.write(fo);
		}
		
		fi = new FileInputStream(path);
		workbook = new XSSFWorkbook(fi);
		if(workbook.getSheetIndex(sheetname)==-1)    // if not exists create new
		{
			workbook.createSheet(sheetname);
		}
		
		sheet = workbook.getSheet(sheetname);
		if(sheet.getRow(rownum)==null)
		{
			sheet.createRow(rownum);
		}
		row = sheet.getRow(rownum);
		
		cell = row.createCell(colnum);
		cell.setCellValue(data);
		
		//to update data in excel
		fo = new FileOutputStream(path);
		workbook.write(fo);
		workbook.close();
		fi.close();
		fo.close();
	}		
}