
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestCase {

	public static void main(String[] args) throws IOException, InterruptedException {
		WebDriver driver;

		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("https://www.redbus.com/");

		driver.findElement(By.id("src")).sendKeys("Mumbai");
		Thread.sleep(2000);
		driver.findElement(By.id("dest")).sendKeys("Pune");
		Thread.sleep(2000);
		driver.findElement(
				By.cssSelector("#search > div > div.fl.search-box.date-box.gtm-onwardCalendar > div > label")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@class='rb-calendar']//child::tr//child::td[@class='current day']")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("search_btn")).click();
		Thread.sleep(5000);

		List<WebElement> travels = driver.findElements(By.xpath("//div[contains(@class,'travels')]"));
		List<WebElement> travelsTimes = driver.findElements(By.xpath("//div[contains(@class,'dp-time')]"));
		List<WebElement> travelsRatings = driver
				.findElements(By.xpath("//div[contains(@class,'lh-18 rating rat-green')]"));
		List<WebElement> travelsPrices = driver.findElements(By.xpath("//span[contains(@class,'f-19 f-bold')]"));

		String path = ".\\Data.xlsx";
		Files fil = new Files(path);

		for (int i = 0; i < travels.size(); i++) {
			WebElement travel = (WebElement) travels.get(i);
			String s1 = travel.getText();
			System.out.println(s1);

			WebElement travelTime = (WebElement) travelsTimes.get(i);
			String s2 = travelTime.getText();
			System.out.println(s2);

			WebElement travelRating = (WebElement) travelsRatings.get(i);
			String s3 = travelRating.getText();
			System.out.println(s3);

			WebElement travelPrice = (WebElement) travelsPrices.get(i);
			String s4 = travelPrice.getText();
			System.out.println(s4);

			fil.setcellData("Sheet1", i, 0, s1);
			fil.setcellData("Sheet1", i, 1, s2);
			fil.setcellData("Sheet1", i, 2, s3);
			fil.setcellData("Sheet1", i, 3, s4);
		}
		driver.close();
	}
}