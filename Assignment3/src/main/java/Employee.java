
public class Employee implements Comparable<Employee>{
	public String name;
	public String position;
	public String office;
	public String age;
	public String startDate;
	public String salary;
	
	public int compareTo(Employee o) {
		if(Integer.parseInt(age)>Integer.parseInt(o.age) )
			return 1;
		else {
			if(Integer.parseInt(age)<Integer.parseInt(o.age))
				return -1;
		}	
		
		return 0;
	}
	
	@Override
	public String toString() {
		return name + "|" + position + "|" + office + "|" + age + "|" + startDate + "|" + salary;
	}
	
}
