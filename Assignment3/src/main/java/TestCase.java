import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestCase {
	static WebDriver driver;

	public static void main(String[] args) {

		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("https://datatables.net/");

		Select Dropdown = new Select(driver.findElement(By.name("example_length")));

		Dropdown.selectByValue("25");

		WebElement table = driver.findElement(By.xpath("//table[@id='example']//tbody"));

		ArrayList<Employee> employees = new ArrayList<Employee>();

		System.out.println("Name|Position|Office|Age|StartDate|Salary");

		for (int r = 1; r <= 25; r++) {
			Employee employee = new Employee();

			employee.name = table.findElement(By.xpath("tr[" + r + "]//td[1]")).getText();
			employee.position = table.findElement(By.xpath("tr[" + r + "]//td[2]")).getText();
			employee.office = table.findElement(By.xpath("tr[" + r + "]//td[3]")).getText();
			employee.age = table.findElement(By.xpath("tr[" + r + "]//td[4]")).getText();
			employee.startDate = table.findElement(By.xpath("tr[" + r + "]//td[5]")).getText();
			employee.salary = table.findElement(By.xpath("tr[" + r + "]//td[6]")).getAttribute("value");

			System.out.println(employee);

			employees.add(employee);
		}

		System.out.println("=============Sorted by Age<30 and position = Software Engineer======================");

		driver.findElement(By.xpath("//*[@id=\'example\']/thead/tr/th[4]")).click();

		ArrayList<Employee> employees1 = new ArrayList<Employee>();

		System.out.println("Name|Position|Office|Age|StartDate|Salary");

		for (int r = 1; r <= 25; r++) {
			Employee employee1 = new Employee();

			employee1.name = table.findElement(By.xpath("tr[" + r + "]//td[1]")).getText();
			employee1.position = table.findElement(By.xpath("tr[" + r + "]//td[2]")).getText();
			employee1.office = table.findElement(By.xpath("tr[" + r + "]//td[3]")).getText();
			employee1.age = table.findElement(By.xpath("tr[" + r + "]//td[4]")).getText();
			employee1.startDate = table.findElement(By.xpath("tr[" + r + "]//td[5]")).getText();
			employee1.salary = table.findElement(By.xpath("tr[" + r + "]//td[6]")).getAttribute("value");

			employees1.add(employee1);
		}

		for (Employee employee1 : employees1) {
			if (Integer.parseInt(employee1.age) < 30 && employee1.position.compareTo("Software Engineer") == 0)
				System.out.println(employee1);
		}
		driver.close();
	}
}
