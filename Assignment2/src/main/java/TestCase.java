
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestCase {
	WebDriver driver;
	POM pom;
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	ExtentTest test;

	@BeforeSuite
	public void setup() {
		WebDriverManager.firefoxdriver().setup();
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
	}

	@BeforeClass
	public void OpenBrowser() {
		driver.get("http://www.allmovie.com/");

		pom = new POM(driver);
	}

	@AfterClass
	public void closeBrowser() {
		driver.close();
		extent.flush();
	}

	@BeforeTest
	public void startExtent() {
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/ExtentReport/testReport.html");
		htmlReporter.config().setDocumentTitle("Assignment2");
		htmlReporter.config().setReportName("Test Case Report");
		htmlReporter.config().setTheme(Theme.DARK);

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Hostname", "LocalHost");
		extent.setSystemInfo("OS", "Windows10");

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); // to add name in extent report
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); // to add error/exception in extent
																					// // report

		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
		}
	}

	@Test(priority = 1)
	public void search() {
		test = extent.createTest("search");
		pom.searchMovie();
	}

	@Test(priority = 2)
	public void printNumer() {
		test = extent.createTest("printNumber");
		pom.printResult();
	}

	@Test(priority = 3)
	public void clickMovieLink() {
		test = extent.createTest("clickMovieLink");
		pom.clkLink();
	}

	@Test(priority = 4)
	public void verifyGenereAndRating() {
		test = extent.createTest("verifyGenereAndRating");
		pom.verify();
	}

	@Test(priority = 5)
	public void verifyDir() {
		test = extent.createTest("verifyDir");
		pom.verifyD();
	}

	@Test(priority = 7)
	public void verifyName() {
		test = extent.createTest("verifyName");
		pom.verifyNameC();
	}
}
