import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class POM {

	private WebDriver driver;

	@FindBy(xpath = "//input[contains(@class,'site-search-input')]")
	WebElement input;

	@FindBy(className = "site-search-button")
	WebElement submit;

	@FindBy(xpath = "//*[@id=\'cmn_wrap\']/div[2]/div[2]/div/h1")
	WebElement print;

	@FindBy(xpath = "//*[@id=\'cmn_wrap\']/div[2]/div[2]/div/ul/li[1]/div[2]/div[1]/a")
	WebElement clk;

	@FindBy(xpath = "//*[@id=\'cmn_wrap\']/div[2]/div[2]/header/div/span[1]/a")
	WebElement txt1;

	@FindBy(xpath = "//*[@id=\'cmn_wrap\']/div[2]/div[2]/header/div/span[6]/span")
	WebElement txt2;

	@FindBy(xpath = "//*[@id=\'cmn_wrap\']/div[2]/div[2]/ul/li[4]/a")
	WebElement btn;

	@FindBy(xpath = "//a[(text()='Francis Ford Coppola')]")
	WebElement dir;

	@FindBy(xpath = "//*[@id=\'cmn_wrap\']/div[2]/div[2]/section/div[2]/div[2]/div[3]")
	WebElement name;

	public POM(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(this.driver, this);
	}

	public void searchMovie() {
		input.sendKeys("The Godfather");
		submit.submit();
	}

	public void printResult() {
		String txt = print.getText();
		System.out.println(txt);
	}

	public void clkLink() {
		clk.click();
	}

	public void verify() {
		String s1 = txt1.getText();
		Assert.assertEquals(s1, "Crime");

		String s2 = txt2.getText();
		Assert.assertEquals(s2, "A");
	}

	public void verifyD() {
		btn.click();

		String s1 = dir.getText();
		Assert.assertEquals(s1, "Francis Ford Coppola");
	}

	public void verifyNameC() {
		String s1 = name.getText();
		Assert.assertEquals(s1, "Michael Corleone");
	}
}
