import java.io.File;
import java.util.concurrent.TimeoutException;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class MapPOM {
	private WebDriver driver;
	private WebDriverWait wait;

	@FindBy(id = "searchboxinput")
	WebElement searchTxtBox;

	@FindBy(xpath = "//button[@id='searchbox-searchbutton']")
	WebElement searchBtn;

	@FindBy(xpath = "//button[@jsaction='pane.rating.category']")
	WebElement getTxt;

	@FindBy(className = "aMPvhf-fI6EEc-KVuj8d")
	WebElement ratings;

	@FindBy(className = "CsEnBe")
	WebElement address;

	@FindBy(xpath = "//button[@data-tooltip=\"Open website\"]/div/div[2]/div")
	WebElement varifyLink;

	@FindBy(className = "widget-pane-link")
	WebElement reviews;

	@FindBy(className = "lRsTH-Tswv1b-text")
	WebElement number;

	public MapPOM(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;

		PageFactory.initElements(this.driver, this);
	}

	public void searchPlace() throws TimeoutException, InterruptedException {
		searchTxtBox.sendKeys("Wankhede Stadium");
		searchBtn.click();

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Wankhede Stadium')]")));
		Thread.sleep(5000);
	}

	public void takeSnapshot() throws Exception {
		TakesScreenshot scr = (TakesScreenshot) driver;
		File f = scr.getScreenshotAs(OutputType.FILE);
		FileHandler.copy(f, new File("./ScreenShot\\Search.png"));
	}

	public void getTxt() {
		String s1 = getTxt.getText();
		Assert.assertEquals(s1, "Stadium");
	}

	public void varifyTtl() throws Exception {
		String Title = driver.getTitle();
		String s1 = new String("Wankhede Stadium Mumbai (Cricket stadium) - Google Maps");

		Assert.assertEquals(Title, s1);
		Thread.sleep(2000);
	}

	public void ratingReview() {
		String s1 = ratings.getText();
		String s2 = reviews.getText();
		System.out.println("ratings " + s1 + " reviews " + s2);
	}

	public void vrfyLink() {
		String s1 = varifyLink.getText();
		String s2 = new String("mumbaicricket.com");
		Assert.assertEquals(s1, s2);
	}

	public void printAddr() {
		String s1 = address.getText();
		System.out.println("Address " + s1);
	}

	public void checkNumber() {
		String s1 = number.getText();
		Assert.assertEquals(s1, "022 2279 5500");
	}

	public void instantSnapshot() throws Exception {
		WebElement scr = driver.findElement(By.className("lRsTH-Tswv1b-JIbuQc-LgbsSe"));

		File f = scr.getScreenshotAs(OutputType.FILE);
		FileHandler.copy(f, new File("./ScreenShot\\Number.png"));
	}
}
