
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import io.github.bonigarcia.wdm.WebDriverManager;

@Test
public class TestCase {
	WebDriver driver;
	MapPOM mapPOM;
	WebDriverWait wait;
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	ExtentTest test;

	@BeforeSuite
	public void setup() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();

		driver.manage().window().maximize();
		this.wait = new WebDriverWait(this.driver, 30);
	}

	@BeforeClass
	public void OpenBrowser() throws InterruptedException {
		driver.get("https://www.google.com/maps/");

		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@aria-label='Search Google Maps']")));

		mapPOM = new MapPOM(driver, wait);
	}

	@AfterClass
	public void closeBrowser() {
		driver.close();
		extent.flush();
	}

	@BeforeTest
	public void startExtent() {
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/ExtentReport/testReport.html");
		htmlReporter.config().setDocumentTitle("Assignment1");
		htmlReporter.config().setReportName("Test Case Report");
		htmlReporter.config().setTheme(Theme.DARK);

		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Hostname", "LocalHost");
		extent.setSystemInfo("OS", "Windows10");

	}

	@AfterMethod
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getName()); // to add name in extent report
			test.log(Status.FAIL, "TEST CASE FAILED IS " + result.getThrowable()); // to add error/exception in extent
																					// report

		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(Status.SKIP, "Test Case SKIPPED IS " + result.getName());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, "Test Case PASSED IS " + result.getName());
		}
	}

	@Test
	public void searchLocation() throws Exception {
		test = extent.createTest("searchLocation");
		mapPOM.searchPlace();

		mapPOM.takeSnapshot();
	}

	@Test(dependsOnMethods = { "searchLocation" })
	public void varifyText() {
		test = extent.createTest("varifyText");
		mapPOM.getTxt();
	}

	@Test(dependsOnMethods = { "varifyText" })
	public void varifyTitle() throws Exception {
		test = extent.createTest("varifyTitle");
		mapPOM.varifyTtl();
	}


	@Test(dependsOnMethods = { "varifyTitle" })
	public void printRatingsAndReviews() {
		test = extent.createTest("printRatingsAndReviews");
		mapPOM.ratingReview();
	}

	@Test(dependsOnMethods = { "printRatingsAndReviews" })
	public void verifyLink() throws Exception {
		test = extent.createTest("verifyLink");
		mapPOM.vrfyLink();
	}

	@Test(dependsOnMethods = { "verifyLink" })
	public void printAddress() throws Exception {
		test = extent.createTest("printAddress");
		mapPOM.printAddr();
	}

	@Test(dependsOnMethods = { "printAddress" })
	public void contactNumber() throws Exception {
		test = extent.createTest("contactNumber");
		try {
			mapPOM.checkNumber();
		} finally {
			mapPOM.instantSnapshot();
		}
	}
}
